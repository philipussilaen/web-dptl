<!DOCTYPE html>
 <html lang="en">
 <body>
    <section id="container" >
<?php 
    include 'header.php';
    
    $isNew = true;
    
    $data = null;
    
    if(isset($_GET['id']) != ''){
        $isNew = false;

        $stmt = $connect->prepare('SELECT code, label, description FROM kategori WHERE code = :id');
        $stmt->execute(array(
            ':id' => $_GET['id']
        ));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
    }
        
	if(isset($_POST['submit'])) {
		$errMsg = '';

		// Getting data from FROM
		$code = $_POST['code'];
		$label = $_POST['label'];
		$desc = $_POST['description'];
		
				
		if($errMsg == '') {
			try {
			    if ($isNew){
			        $stmt = $connect->prepare('INSERT INTO kategori (code, label, description) VALUES (:code, :label, :description)');
			        $stmt->execute(array(
			            ':code' => $code,
			            ':label' => $label,
			            ':description' => $desc
			        ));
			        echo "<script>location.href='category_management.php?action=Category successfully created';</script>";
			    }else{
			        $sql = "UPDATE kategori SET label = :label, description = :description WHERE code = :code";
			        $stmt = $connect->prepare($sql);
			        $stmt->execute(array(
			            ':code' => $code,
			            ':label' => $label,
			            ':description' => $desc
			        ));
			        echo "<script>location.href='category_management.php?action=Category successfully updated';</script>";
			    }
			}
			catch(PDOException $e) {
				$errMsg = $e->getMessage();
			}
		}
	}
?>

      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> Form Category</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <div class="col-lg-12">
              	<h4><i class="fa fa-angle-right"></i> Category</h4>
              </div>
              <section id="unseen">
              	<div class="form-panel">
					<form class="form-horizontal style-form" method="post" enctype = "multipart/form-data" action="">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Code</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" <?php if ($data != null) echo "readonly" ?> name="code" value="<?php if ($data != null) echo $data['code']; ?>" />
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Label</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="label" value="<?php if($data != null) echo $data['label']; ?>" autocomplete="off" class="box"/>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Description</label>
                              <div class="col-sm-10">
                              	  <textarea class="form-control" name="description" rows="4" cols="2"><?php if($data != null) echo $data['description']; ?></textarea>
                              </div>
                          </div>

                          
                          <a href="category_management.php" class='submit btn btn-danger'>Cancel</a>
                    	  <input type="submit" name='submit' value="Update" class='submit btn btn-primary'/><br />
                    	<br />
                       	<?php
        				    if(isset($errMsg)){					
        					   echo '<div class="alert alert-danger">'.$errMsg.'</div>';
        				    }
			             ?>
                     </form>
                   </div>
              </section>
            </div>
          </div>
         </div>
            <!-- /content-panel -->
          </section>
        </section>

        <!--main content end-->
        <?php include 'footer.php'; ?>
      </section>
    </body>
</html>
