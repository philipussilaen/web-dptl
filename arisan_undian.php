 <!DOCTYPE html>
 <html lang="en">
</head>
<body>
  <section id="container" >
    <?php include 'header.php'; 
    $isNew = true;
    
    $data = null;
    
    if(isset($_GET['id_arisan']) != ''){
      $isNew = false;
      $id_arisan = $_GET['id_arisan'];
      $bulan = $_GET['bulan'];
      $tahun = $_GET['tahun'];
    }
    ?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      ****************************s******************************************************************************************************************************* -->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <h3><i class="fa fa-angle-right"></i> Undi Arisan</h3>
          <div class="row mt">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <! -- BASIC BUTTONS -->
              <div class="showback">
                <?php echo '<h4 class="mb"><i class="fa fa-angle-right"></i> Undi Arisan RT '. $id_arisan .'</h4>'; ?>
                <table class="table table-bordered table-striped table-condensed">
                  <thead>
                    <tr>
                      <th>Nama</th>
                      <th>RT</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($connect->query("SELECT fullname, id_kelompok FROM user WHERE role = 'sosialita' AND id_kelompok = ".$id_arisan."") as $rows) {
                      echo '<tr>';
                      echo '<td>' . $rows['fullname'] . '</td>';
                      echo '<td>' . $rows['id_kelompok'] . '</td>';
                      echo '</tr>';
                    }
                    ?>              
                  </tbody>
                </table>
              </div><!-- /showback -->
            </div><! --/col-lg-6 -->
            <div class="col-lg-6 col-md-6 col-sm-12">
              <! -- BUTTONS SIZES -->
              <div class="showback">
               <?php echo '<h4 class="mb"><i class="fa fa-angle-right"></i> '.$bulan.' '.$tahun.'</h4>'; ?>
               <?php echo '<a class="btn btn-primary btn-lg btn-block" href="arisan_hasil.php?id_arisan='.$id_arisan.'&bulan='.$bulan.'&tahun='.$tahun.'">Undi !!!</a>'; ?>
             </div><!-- /showback -->
           </div><!-- /col-lg-6 -->
         </div><!--/ row -->
       </section><! --/wrapper -->
     </section><!-- /MAIN CONTENT -->
     <!--main content end-->
     <?php include 'footer.php'; ?>
   </section>


   <!--script for this page-->
   <script src="assets/js/sparkline-chart.js"></script>    
   <script src="assets/js/zabuto_calendar.js"></script>  

      <!-- <script type="text/javascript">
        $(document).ready(function () {
          var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Welcome to Dashgum!',
            // (string | mandatory) the text inside the notification
            text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
            // (string | optional) the image to display on the left
            image: 'assets/img/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
          });

          return false;
        });
      </script> -->

      <script type="application/javascript">
        $(document).ready(function () {
          $("#date-popover").popover({html: true, trigger: "manual"});
          $("#date-popover").hide();
          $("#date-popover").click(function (e) {
            $(this).hide();
          });

          $("#my-calendar").zabuto_calendar({
            action: function () {
              return myDateFunction(this.id, false);
            },
            action_nav: function () {
              return myNavFunction(this.id);
            },
            ajax: {
              url: "show_data.php?action=1",
              modal: true
            },
            legend: [
            {type: "text", label: "Special event", badge: "00"},
            {type: "block", label: "Regular event", }
            ]
          });
        });


        function myNavFunction(id) {
          $("#date-popover").hide();
          var nav = $("#" + id).data("navigation");
          var to = $("#" + id).data("to");
          console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
      </script>


    </body>
    </html>
