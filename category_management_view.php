<!DOCTYPE html>
 <html lang="en">
 <body>
    <section id="container" >
<?php 
    include 'header.php';
    
    $isNew = true;
    
    $data = null;
    
    if(isset($_GET['id']) != ''){
        $isNew = false;

        $stmt = $connect->prepare('SELECT code, label, description FROM kategori WHERE code = :id');
        $stmt->execute(array(
            ':id' => $_GET['id']
        ));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
    }
        
?>

      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> View Category</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <div class="col-lg-12">
              	<h4><i class="fa fa-angle-right"></i> Category</h4>
              </div>
              <section id="unseen">
              	<div class="form-panel">
					<form class="form-horizontal style-form" method="post" enctype = "multipart/form-data" action="">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Code</label>
                              <div class="col-sm-10">
                                  <input type="hidden" name="no" value="<?php if ($data != null) echo $data['code']; ?>" />
                                  <?php if($data != null) echo $data['code']; ?>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Label</label>
                              <div class="col-sm-10">
                                  <input type="hidden" name="no" value="<?php if ($data != null) echo $data['label']; ?>" />
                                  <?php if($data != null) echo $data['label']; ?>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Label</label>
                              <div class="col-sm-10">
                                  <input type="hidden" name="no" value="<?php if ($data != null) echo $data['description']; ?>" />
                                  <?php if($data != null) echo $data['description']; ?>
                              </div>
                          </div>
                          <a href="index.php" class='submit btn btn-danger'>Cancel</a>
                     </form>
                   </div>
              </section>
            </div>
          </div>
         </div>
            <!-- /content-panel -->
          </section>
        </section>

        <!--main content end-->
        <?php include 'footer.php'; ?>
      </section>
    </body>
</html>
