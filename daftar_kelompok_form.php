<!DOCTYPE html>
<html lang="en">
<body>
  <section id="container" >
    <?php 
    include 'header.php';
    
    $isNew = true;
    
    $data = null;
    
    if(isset($_GET['id_kelompok']) != ''){
      $isNew = false;

      $stmt = $connect->prepare('SELECT id_kelompok, ketua, kontak, rt, rw FROM kelompok_warga WHERE id_kelompok = :id_kelompok');
      $stmt->execute(array(
        ':id_kelompok' => $_GET['id_kelompok']
      ));
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
    }

    if(isset($_POST['submit'])) {
      $errMsg = '';

		  // Getting data from FROM
      $id_kelompok = $_POST['id_kelompok'];
      $ketua = $_POST['ketua'];
      $kontak = $_POST['kontak'];
      $rt = $_POST['rt'];
      $rw = $_POST['rw'];

      if($errMsg == '') {
       try {
         if ($isNew){
           $stmt = $connect->prepare('INSERT INTO kelompok_warga (ketua, kontak, rt, rw) VALUES (:ketua, :kontak, :rt, :rw)');
           $stmt->execute(array(
             ':ketua' => $ketua,
             ':kontak' => $kontak,
             ':rt' => $rt,
             ':rw' => $rw
           ));
           echo "<script>location.href='daftar_kelompok_warga.php';</script>";
         }else{
           $sql = "UPDATE kelompok_warga SET ketua = :ketua, kontak = :kontak, rt = :rt, rw = :rw WHERE id_kelompok = :id_kelompok";
           $stmt = $connect->prepare($sql);
           $stmt->execute(array(
            ':ketua' => $ketua,
            ':kontak' => $kontak,
            ':rt' => $rt,
            ':rw' => $rw,
            ':id_kelompok' => $id_kelompok
          ));
           echo "<script>location.href='daftar_kelompok_warga.php';</script>";
         }
       }
       catch(PDOException $e) {
        $errMsg = $e->getMessage();
      }
    }
  }
  ?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> Form Kelompok Warga</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <div class="col-lg-12">
              	<h4><i class="fa fa-angle-right"></i> Kelompok Warga</h4>
              </div>
              <section id="unseen">
              	<div class="form-panel">
                 <form class="form-horizontal style-form" method="post" action="">
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Ketua</label>
                    <div class="col-sm-10">
                      <input type="hidden" name="id" value="<?php if ($data != null) echo $data['id_kelompok']; ?>" />
                      <input type="text" class="form-control" name="ketua" value="<?php if($data != null) echo $data['ketua']; ?>" autocomplete="off" class="box"/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Kontak</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="kontak" value="<?php if($data != null) echo $data['kontak']; ?>" autocomplete="off" class="box"/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">RT</label>
                    <div class="col-sm-10">
                      <input type="integer" class="form-control" name="rt" value="<?php if($data != null) echo $data['rt']; ?>" autocomplete="off" class="box"/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">RW</label>
                    <div class="col-sm-10">
                      <input type="integer" class="form-control" name="rw" value="<?php if($data != null) echo $data['rw'] ?>" autocomplete="off" class="box" />
                    </div>
                  </div>
                  <a href="daftar_kelompok_warga.php" class='submit btn btn-danger'>Cancel</a>
                  <input type="submit" name='submit' value="Update" class='submit btn btn-primary'/><br />
                  <br />
                  <?php
                  if(isset($errMsg)){					
                    echo '<div class="alert alert-danger">'.$errMsg.'</div>';
                  }
                  ?>
                </form>
              </div>
            </section>
          </div>
        </div>
      </div>
      <!-- /content-panel -->
    </section>
  </section>

  <!--main content end-->
  <?php include 'footer.php'; ?>
</section>
</body>
</html>
