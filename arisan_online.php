 <!DOCTYPE html>
 <html lang="en">
</head>
<body>
  <section id="container" >
    <?php include 'header.php'; ?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      ****************************s******************************************************************************************************************************* -->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <div class="row mt">
            <div class="col-lg-12 col-md-6 col-sm-12">
              <div class="showback">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Daftar Arisan RT</h4>
                <table class="table table-bordered table-striped table-condensed">
                  <thead>
                    <tr>
                      <th>RT</th>
                      <th>Periode</th>
                      <th>Pemegang Undian</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    foreach ($connect->query("SELECT * FROM arisan_online ORDER BY id_arisan") as $rows) {
                      $dateElements = explode('-', $rows['periode']);

                      switch ($dateElements[1]){  
                        case '01'    :  $mo = "January"; break;
                        case '02'    :  $mo = "February";break;
                        case '03'    :  $mo = "March";  break;
                        case '04'    :  $mo = "April"; break;
                        case '05'    :  $mo = "May"; break;
                        case '06'    :  $mo = "June"; break;
                        case '07'    :  $mo = "July"; break;
                        case '08'    :  $mo = "August"; break;
                        case '09'    :  $mo = "September"; break;
                        case '10'    :  $mo = "October";break;  
                        case '11'    :  $mo = "November";break;
                        case '12'    :  $mo = "December"; break;
                      }

                      echo '<tr>';
                      echo '<td>' . $rows['erte'] . '</td>';
                      echo '<td>' . $mo.' '.$dateElements[0] . '</td>';
                      echo '<td>' . $rows['pemegang'] . '</td>';

                      echo '<td>
                      <a class="btn btn-theme03" href="arisan_undian.php?id_arisan='.$rows['id_arisan'].'&bulan='.$mo.'&tahun='.$dateElements[0].'">
                      <i class="glyphicon glyphicon-glass">
                      </i> Undi</a> 
                      </td>';
                      echo '</tr>';
                    }
                    ?>
                  </tbody>
                </table>
              </div><!-- /showback -->
            </div><! --/col-lg-6 -->
          </div><!--/ row -->

        </section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->
      <!--main content end-->
      <?php include 'footer.php'; ?>
    </section>


    <!--script for this page-->
    <script src="assets/js/sparkline-chart.js"></script>    
    <script src="assets/js/zabuto_calendar.js"></script>  

      <!-- <script type="text/javascript">
        $(document).ready(function () {
          var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Welcome to Dashgum!',
            // (string | mandatory) the text inside the notification
            text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
            // (string | optional) the image to display on the left
            image: 'assets/img/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
          });

          return false;
        });
      </script> -->

      <script type="application/javascript">
        $(document).ready(function () {
          $("#date-popover").popover({html: true, trigger: "manual"});
          $("#date-popover").hide();
          $("#date-popover").click(function (e) {
            $(this).hide();
          });

          $("#my-calendar").zabuto_calendar({
            action: function () {
              return myDateFunction(this.id, false);
            },
            action_nav: function () {
              return myNavFunction(this.id);
            },
            ajax: {
              url: "show_data.php?action=1",
              modal: true
            },
            legend: [
            {type: "text", label: "Special event", badge: "00"},
            {type: "block", label: "Regular event", }
            ]
          });
        });


        function myNavFunction(id) {
          $("#date-popover").hide();
          var nav = $("#" + id).data("navigation");
          var to = $("#" + id).data("to");
          console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
      </script>


    </body>
    </html>
