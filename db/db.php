<?php
/*
 * A PHP MySQL PDO class 
 */
session_start();

// Define database local
define('dbhost', 'localhost');
define('dbuser', 'root');
define('dbpass', 'envdev123');
define('dbname', 'db_erte');

// define('dbhost', 'localhost');
// define('dbuser', 'root');
// define('dbpass', 'envdev123');
// define('dbname', 'db_erte');

// Connecting database
try {
    $connect = new PDO("mysql:host=".dbhost."; dbname=".dbname, dbuser, dbpass);
    $connect->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch(PDOException $e) {
    echo $e->getMessage();
}

?>
