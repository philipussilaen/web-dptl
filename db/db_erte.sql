-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2018 at 07:50 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_erte`
--

-- --------------------------------------------------------

--
-- Table structure for table `kelompok_warga`
--

CREATE TABLE `kelompok_warga` (
  `id_kelompok` int(255) NOT NULL,
  `ketua` varchar(999) NOT NULL,
  `kontak` varchar(999) NOT NULL,
  `jumlah` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelompok_warga`
--

INSERT INTO `kelompok_warga` (`id_kelompok`, `ketua`, `kontak`, `jumlah`) VALUES
(1, 'Lalala', '08123', 5),
(2, 'Lilili', '08234', 8),
(3, 'Lololo', '08345', 10);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `code` varchar(100) NOT NULL,
  `label` varchar(100) NOT NULL,
  `icon` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `parent` varchar(100) DEFAULT NULL,
  `order_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`code`, `label`, `icon`, `location`, `parent`, `order_no`) VALUES
('', 'Daftar Kelompok Warga', 'glyphicon glyphicon-user', 'daftar_kelompok_warga.php', NULL, 2),
('USM', 'User Management', 'glyphicon glyphicon-user', 'user_management.php', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `role`, `fullname`, `email`) VALUES
(1, 'daniel', 'hasugian', 'member', 'Daniel', 'daniel@gmail.com'),
(2, 'zamroji', 'P4ssword', 'administrator', 'Zamrozi', 'zamrozi@gmail.com'),
(3, 'dany', 'P4ssword', 'administrator', 'dany', 'Danielgamil.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kelompok_warga`
--
ALTER TABLE `kelompok_warga`
  ADD PRIMARY KEY (`id_kelompok`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`code`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kelompok_warga`
--
ALTER TABLE `kelompok_warga`
  MODIFY `id_kelompok` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
