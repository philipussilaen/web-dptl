<!DOCTYPE html>
<html lang="en">
<body>
 <section id="container" >

  <?php 
  include 'header.php';
  $data = null;
  $ketua = '';

  if(isset($_GET['id_kelompok']) != ''){
  $stmt = $connect->prepare('SELECT ketua FROM kelompok_warga WHERE id_kelompok = :id_kelompok');
  $stmt->execute(array(
  ':id_kelompok' => $_GET['id_kelompok']
  ));
  $data = $stmt->fetch(PDO::FETCH_ASSOC);
  $ketua = $data['ketua'];
}else {
echo "<script>location.href=daftar_kelompok_warga.php?action='Invalid Kelompok'</script>";
}

if(isset($_POST['submit'])) {
$errMsg = '';

try{
$stmt = $connect->prepare('DELETE FROM kelompok_warga WHERE id_kelompok = :id_kelompok LIMIT 1');
$stmt->execute(array(
':id_kelompok' => $_GET['id_kelompok']
));
echo "<script>location.href='daftar_kelompok_warga.php?action=Kelompok successfully removed.'</script>";

}
catch(PDOException $e) {
$errMsg = $e->getMessage();
}
}
?>

<section id="main-content">
  <section class="wrapper">
    <h3><i class="fa fa-angle-right"></i> Delete Kelompok Warga</h3>
    <div class="row mt">
      <div class="col-lg-12">
        <div class="content-panel">
          <div class="col-lg-12">
           <h4><i class="fa fa-angle-right"></i> Kelompok Warga</h4>
         </div>
         <section id="unseen">
           <div class="form-panel">
             <form class="form-horizontal style-form" method="post" action="">
              <div class="form-group">
                <label class="col-sm-2 col-sm-2 control-label">Ketua</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="ketua" disabled value="<?php echo $ketua ?>" autocomplete="off" class="box"/>
                </div>
              </div>
              <a href="daftar_kelompok_warga.php" class='submit btn btn-danger'>Cancel</a>
              <input type="submit" name='submit' value="Delete" class='submit btn btn-primary'/><br />
              <br />
              <?php
              if(isset($errMsg)){					
              echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            }
            ?>
          </form>
        </div>
      </section>
    </div>
  </div>
</div>
<!-- /content-panel -->
</section>
</section>

<!--main content end-->
<?php include 'footer.php'; ?>
</section>
</body>
</html>
