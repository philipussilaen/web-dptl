<!DOCTYPE html>
 <html lang="en">
 <head>
         <link rel="stylesheet" type="text/css" href="assets/gallery/css/demo.css" />
        <link rel="stylesheet" type="text/css" href="assets/gallery/css/style.css" />
		<script type="text/javascript" src="assets/gallery/js/modernizr.custom.53451.js"></script>
 </head>
 <body>
    <section id="container" >
      <?php include 'header.php'; ?>
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> Daftar Pasar Online</h3>
                <?php 
                    $kategori = '';
                    if (isset($_GET['kategori']) != ''){
                        $kategori = $_GET['kategori'];
                    }
                ?>
		 <div class="col-lg-12">
     		<section id="dg-container" class="dg-container">
    			<div class="dg-wrapper">
    				<a href="#"><img height="300px" src="images/slider/gambar1.jpg" alt="image01"><div>Mari Promosikan Barang Anda dan Jadilah Kerabat Kami</div></a>
    				<a href="#"><img height="300px" width="480px" src="images/slider/gambar2.jpg" alt="image02"><div>Mari Promosikan Barang Anda dan Jadilah Kerabat Kami</div></a>
    				<a href="#"><img height="300px" width="480px" src="images/slider/gambar3.jpg" alt="image03"><div>Mari Promosikan Barang Anda dan Jadilah Kerabat Kami</div></a>
    				<a href="#"><img height="300px" width="480px" src="images/slider/gambar4.jpg" alt="image04"><div>Mari Promosikan Barang Anda dan Jadilah Kerabat Kami</div></a>
    				<a href="#"><img height="300px" width="480px" src="images/slider/gambar5.jpg" alt="image05"><div>Mari Promosikan Barang Anda dan Jadilah Kerabat Kami</div></a>
    				<a href="#"><img height="300px" width="480px" src="images/slider/gambar6.jpg" alt="image06"><div>Mari Promosikan Barang Anda dan Jadilah Kerabat Kami</div></a>
    				<a href="#"><img height="300px" width="480px" src="images/slider/gambar7.jpg" alt="image07"><div>Mari Promosikan Barang Anda dan Jadilah Kerabat Kami</div></a>
    				<a href="#"><img height="300px" width="480px" src="images/slider/gambar8.jpg" alt="image08"><div>Mari Promosikan Barang Anda dan Jadilah Kerabat Kami</div></a>
    				<a href="#"><img height="300px" width="480px" src="images/slider/gambar9.jpg" alt="image09"><div>Mari Promosikan Barang Anda dan Jadilah Kerabat Kami</div></a>
    				<a href="#"><img height="300px" width="480px" src="images/slider/gambar10.jpg" alt="image10"><div>Mari Promosikan Barang Anda dan Jadilah Kerabat Kami</div></a>
    			</div>
    		</section>
         </div>
         <div class="col-lg-8" style="margin-top: -50px">
          	<form class="form-horizontal style-form" method="GET" action="">
          	  <div class="form-group">
          		<div class=" col-md-6">
                  <select class="form-control" name="kategori" class="form-control">
                    <?php
                      foreach ($connect->query("SELECT * FROM kategori") as $rows) {
                          echo '<option value="'.$rows['code'].'">'.$rows['label'].'</option>';
                      }
                    ?>    
					</select>
				</div>
				<input type="submit" name='submit' value="Search" class='submit btn btn-primary'/><br />
    		   </div>
			</form>
		 </div>
         <div class="row col-md-12 mt" style="">
             <?php
              $i = 0;
              foreach ($connect->query("SELECT * FROM pasar_online where kategori like '%".$kategori."%'") as $rows) {
                $i++;
                echo '<div class="col-md-4 col-sm-4 mb"><div class="white-panel pn" ><div class="white-header" style="background-color:#428bca; color:#fff;"><h5>'.$rows['title'].'</h5></div><div class="row"><div class="col-sm-6 col-xs-6 goleft" style="color:#000;"><p>'.$rows['price'].'</p></div><div class="col-sm-6 col-xs-6"></div></div><div class="centered"><img src="'.$rows['image_path'].'" width="120"></div></div><div><a href="pasar_online_management_view.php?id='.$rows['no'].'" type="button" class="btn btn-primary" style="margin-top:10px;" ><i class="glyphicon glyphicon-arrow-right"></i> Detail</a></div></div>';
              }
            ?>   
            </div>
            <!-- /content-panel -->
          </section>
        </section>

        <!--main content end-->
        <?php include 'footer.php'; ?>
      </section>
   	</body>
   		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
		<script type="text/javascript" src="assets/gallery/js/jquery.gallery.js"></script>
		<script type="text/javascript">
			$(function() {
				$('#dg-container').gallery({
					autoplay	:	true
				});
			});
		</script>
</html>
