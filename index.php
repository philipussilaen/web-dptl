<!DOCTYPE html>
<html lang="en">
<body>

 <!--external css-->
 <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
 <link href="assets/js/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
 <section id="container" >
  <?php include 'header.php'; ?>

  <?php 

  $news = $connect->query('SELECT no, title, content, image_path FROM news');

  ?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper site-min-height">
          <h3><i class="fa fa-angle-right"></i> Home </h3>
          <hr>
          <!--news start-->
        </div>
        <div class="row mt"> 
          <div class="col-lg-12">
            <div class="form-panel">
              <h4 class="mb"><i class="fa fa-angle-right"></i> News Update</h4>
              <div class="row mt">
                <?php
                foreach ($connect->query("SELECT * FROM news") as $newss) {
                  ?>
                  <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 desc">
                   <div class="project-wrapper">
                    <div class="project">
                      <div class="photo-wrapper">
                        <div class="photo">
                          <a class="fancybox"><img class="img-responsive" height="150" width="170" src="<?php echo $newss['image_path']?>" alt=""></a>
                        </div>
                        <div class="overlay"></div>
                      </div>
                    </div>
                  </div>
                </div><!-- col-lg-4 -->
                <div class="col-lg-2 col-md-3 col-sm-3 col-xs-12 desc">
                  <div class="project-wrapper">
                    <div class="project">
                      <?php echo $newss['title']?>
                    </div>
                    <a href="news_management_view.php?id=<?php echo $newss['no']?>">read more...</a>
                  </div>
                </div><!-- col-lg-4 -->
                <?php  } ?>
              </div>   
            </div>
          </div><!-- col-lg-12-->   
        </div><!-- /row -->
        <!--news end-->
        <div class="row mt"> 
          <div class="col-lg-12">
            <h3><i class="fa fa-angle-right"></i> Events Update</h3>
            <!-- page start-->
            <div class="row mt">
              <aside class="col-lg-6 mt">
                <section class="panel">
                  <div class="panel-body">
                    <div id="calendar" class="has-toolbar"></div>
                  </div>
                </section>
              </aside>
            </div>
            <!-- page end-->
          </div>
        </div>
        <!--gallery start-->
        <div class="row mt"> 
          <div class="col-lg-12">
            <div class="form-panel">
              <h4 class="mb"><i class="fa fa-angle-right"></i> Gallery Update</h4>
              <div class="row mt">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 desc">
                  <div class="project-wrapper">
                    <div class="project">
                      <div class="photo-wrapper">
                        <div class="photo">
                          <a class="fancybox" href="assets/img/portfolio/port01.jpg"><img class="img-responsive" src="assets/img/portfolio/port01.jpg" alt=""></a>
                        </div>
                        <div class="overlay"></div>
                      </div>
                    </div>
                  </div>
                </div><!-- col-lg-4 -->

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 desc">
                  <div class="project-wrapper">
                    <div class="project">
                      <div class="photo-wrapper">
                        <div class="photo">
                          <a class="fancybox" href="assets/img/portfolio/port02.jpg"><img class="img-responsive" src="assets/img/portfolio/port02.jpg" alt=""></a>
                        </div>
                        <div class="overlay"></div>
                      </div>
                    </div>
                  </div>
                </div><!-- col-lg-4 -->

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 desc">
                  <div class="project-wrapper">
                    <div class="project">
                      <div class="photo-wrapper">
                        <div class="photo">
                          <a class="fancybox" href="assets/img/portfolio/port03.jpg"><img class="img-responsive" src="assets/img/portfolio/port03.jpg" alt=""></a>
                        </div>
                        <div class="overlay"></div>
                      </div>
                    </div>
                  </div>
                </div><!-- col-lg-4 -->
              </div><!-- /row -->

              <div class="row mt mb">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 desc">
                  <div class="project-wrapper">
                    <div class="project">
                      <div class="photo-wrapper">
                        <div class="photo">
                          <a class="fancybox" href="assets/img/portfolio/port04.jpg"><img class="img-responsive" src="assets/img/portfolio/port04.jpg" alt=""></a>
                        </div>
                        <div class="overlay"></div>
                      </div>
                    </div>
                  </div>
                </div><!-- col-lg-4 -->

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 desc">
                  <div class="project-wrapper">
                    <div class="project">
                      <div class="photo-wrapper">
                        <div class="photo">
                          <a class="fancybox" href="assets/img/portfolio/port05.jpg"><img class="img-responsive" src="assets/img/portfolio/port05.jpg" alt=""></a>
                        </div>
                        <div class="overlay"></div>
                      </div>
                    </div>
                  </div>
                </div><!-- col-lg-4 -->

                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 desc">
                  <div class="project-wrapper">
                    <div class="project">
                      <div class="photo-wrapper">
                        <div class="photo">
                          <a class="fancybox" href="assets/img/portfolio/port06.jpg"><img class="img-responsive" src="assets/img/portfolio/port06.jpg" alt=""></a>
                        </div>
                        <div class="overlay"></div>
                      </div>
                    </div>
                  </div>
                </div><!-- col-lg-4 -->
              </div><!-- /row -->
            </div>
          </div><!-- col-lg-12-->       
        </div><!-- /row -->
        <!--gallery end-->
      </section><! --/wrapper -->
    </section>

    <!--main content end-->
    <?php include 'footer.php'; ?>
  </section>


  <!--script for this page-->
  <script src="assets/js/sparkline-chart.js"></script>    
  <script src="assets/js/zabuto_calendar.js"></script>  

  <script type="text/javascript">
    $(document).ready(function () {
      var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Welcome to eRTe Online!',
            // (string | mandatory) the text inside the notification
            text: 'Website ini berisi informasi dan kegiatan penting di RT maupun Kelurahan. Anda juga dapat menggunakan fitur menarik seperti belanja dan arisan Online serta memberikan peringatan dalam keadaan darurat.',
            // (string | optional) the image to display on the left
            image: 'assets/img/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
          });

      return false;
    });
  </script>

  <script type="application/javascript">
    $(document).ready(function () {
      $("#date-popover").popover({html: true, trigger: "manual"});
      $("#date-popover").hide();
      $("#date-popover").click(function (e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function () {
          return myDateFunction(this.id, false);
        },
        action_nav: function () {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [
        {type: "text", label: "Special event", badge: "00"},
        {type: "block", label: "Regular event", }
        ]
      });
    });


    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="assets/js/fullcalendar/fullcalendar.min.js"></script>    
  <script src="assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


  <!--common script for all pages-->
  <script src="assets/js/common-scripts.js"></script>

  <!--script for this page-->
  <script src="assets/js/calendar-conf-events.js"></script>    

  <script>
      //custom select box

      $(function(){
        $("select.styled").customSelect();
      });

    </script>

  </body>
  </html>
