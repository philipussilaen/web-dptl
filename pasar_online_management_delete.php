<!DOCTYPE html>
 <html lang="en">
 <body>
 <section id="container" >

<?php 
    include 'header.php';
    $data = null;
    
    if(isset($_GET['id']) != ''){
        $stmt = $connect->prepare('SELECT no, title FROM pasar_online WHERE no = :id');
        $stmt->execute(array(
            ':id' => $_GET['id']
        ));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $title = $data['title'];
        $no = $data['no'];
    }else {
        echo "<script>location.href=pasar_online_management.php?action='Invalid News'</script>";
    }
        
	if(isset($_POST['submit'])) {
		$errMsg = '';
		
		try{
		    $stmt = $connect->prepare('DELETE FROM pasar_online WHERE no = :id LIMIT 1');
		    $stmt->execute(array(
		        ':id' => $_GET['id']
		    ));
		    echo "<script>location.href='pasar_online_management.php?action=Pasar Online successfully removed.'</script>";
		    
		}
		catch(PDOException $e) {
		    $errMsg = $e->getMessage();
		}
	}
?>

      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> Form Pasar Online</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <div class="col-lg-12">
              	<h4><i class="fa fa-angle-right"></i> Pasar Online</h4>
              </div>
              <section id="unseen">
              	<div class="form-panel">
					<form class="form-horizontal style-form" method="post" action="">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Title</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="title" disabled value="<?php echo $title ?>" autocomplete="off" class="box"/>
                              </div>
                          </div>
                        <a href="pasar_online_management.php" class='submit btn btn-danger'>Cancel</a>
                    	<input type="submit" name='submit' value="Delete" class='submit btn btn-primary'/><br />
                    	<br />
                       	<?php
        				    if(isset($errMsg)){					
        					   echo '<div class="alert alert-danger">'.$errMsg.'</div>';
        				    }
			             ?>
                     </form>
                   </div>
              </section>
            </div>
          </div>
         </div>
            <!-- /content-panel -->
          </section>
        </section>

        <!--main content end-->
        <?php include 'footer.php'; ?>
      </section>
    </body>
</html>
