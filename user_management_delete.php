<!DOCTYPE html>
 <html lang="en">
 <body>
    <section id="container" >
<?php 
    include 'header.php';
    
    $isNew = true;
    
    $data = null;
    $username = '';
    
    if(isset($_GET['id']) != ''){
        $isNew = false;

        $stmt = $connect->prepare('SELECT id, role, fullname, username, password, email FROM user WHERE id = :id');
        $stmt->execute(array(
            ':id' => $_GET['id']
        ));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $username = $data['username'];
    }else {
        echo "<script>location.href='user_management.php?action='Invalid User'</script>";
    }
        
	if(isset($_POST['submit'])) {
		$errMsg = '';
		
		try{
		    $stmt = $connect->prepare('DELETE FROM user WHERE id = :id LIMIT 1');
		    $stmt->execute(array(
		        ':id' => $_GET['id']
		    ));
		    echo "<script>location.href='user_management.php?action=User successfully removed.'</script>";
		    
		}
		catch(PDOException $e) {
		    $errMsg = $e->getMessage();
		}
	}
?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> Form Pengguna</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <div class="col-lg-12">
              	<h4><i class="fa fa-angle-right"></i> Pengguna</h4>
              </div>
              <section id="unseen">
              	<div class="form-panel">
					<form class="form-horizontal style-form" method="post" action="">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Username</label>
                              <div class="col-sm-10">
                                  <input type="text" class="form-control" name="username" disabled value="<?php echo $username ?>" autocomplete="off" class="box"/>
                              </div>
                          </div>
                        <a href="user_management.php" class='submit btn btn-danger'>Cancel</a>
                    	<input type="submit" name='submit' value="Delete" class='submit btn btn-primary'/><br />
                    	<br />
                       	<?php
        				    if(isset($errMsg)){					
        					   echo '<div class="alert alert-danger">'.$errMsg.'</div>';
        				    }
			             ?>
                     </form>
                   </div>
              </section>
            </div>
          </div>
         </div>
            <!-- /content-panel -->
          </section>
        </section>

        <!--main content end-->
        <?php include 'footer.php'; ?>
      </section>
    </body>
</html>
