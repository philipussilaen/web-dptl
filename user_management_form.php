<!DOCTYPE html>
<html lang="en">
<body>
  <section id="container" >
    <?php 
    include 'header.php';
    
    $isNew = true;
    
    $data = null;
    
    if(isset($_GET['id']) != ''){
      $isNew = false;

      $stmt = $connect->prepare('SELECT id, role, fullname, username, password, email, id_kelompok FROM user WHERE id = :id');
      $stmt->execute(array(
        ':id' => $_GET['id']
      ));
      $data = $stmt->fetch(PDO::FETCH_ASSOC);
    }

    if(isset($_POST['submit'])) {
      $errMsg = '';

		// Getting data from FROM
      $id = $_POST['id'];
      $fullname = $_POST['fullname'];
      $username = $_POST['username'];
      $email = $_POST['email'];
      $password = $_POST['password'];
      $passwordVarify = $_POST['passwordVarify'];
      $role = $_POST['role'];
      $id_kelompok = $_POST['id_kelompok'];

      if($password != $passwordVarify)
       $errMsg = 'Password not matched.';

     if($errMsg == '') {
       try {
         if ($isNew){
           $stmt = $connect->prepare('INSERT INTO user (fullname, username, password, email, role, id_kelompok) VALUES (:fullname, :username, :password, :email, :role, :id_kelompok)');
           $stmt->execute(array(
             ':fullname' => $fullname,
             ':username' => $username,
             ':password' => $password,
             ':email' => $email,
             ':role' => $role,
             ':id_kelompok' => $id_kelompok
           ));
           echo "<script>location.href='user_management.php?action=User successfully created';</script>";
         }else{
           $sql = "UPDATE user SET role = :role, fullname = :fullname, username = :username, password = :password, email = :email, id_kelompok = :id_kelompok WHERE id = :id";
           $stmt = $connect->prepare($sql);
           $stmt->execute(array(
             ':fullname' => $fullname,
             ':username' => $username,
             ':email' => $email,
             ':id_kelompok' => $id_kelompok,
             ':password' => $password,
             ':role' => $role,
             ':id' => $id
           ));
           echo "<script>location.href='user_management.php?action=User successfully updated';</script>";
         }
       }
       catch(PDOException $e) {
        $errMsg = $e->getMessage();
      }
    }
  }
  ?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> Form Pengguna</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <div class="col-lg-12">
              	<h4><i class="fa fa-angle-right"></i> Pengguna</h4>
              </div>
              <section id="unseen">
              	<div class="form-panel">
                 <form class="form-horizontal style-form" method="post" action="">
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Fullname</label>
                    <div class="col-sm-10">
                      <input type="hidden" name="id" value="<?php if ($data != null) echo $data['id']; ?>" />
                      <input type="text" class="form-control" name="fullname" value="<?php if($data != null) echo $data['fullname']; ?>" autocomplete="off" class="box"/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" name="username" value="<?php if($data != null) echo $data['username']; ?>" autocomplete="off" class="box"/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" name="email" value="<?php if($data != null) echo $data['email']; ?>" autocomplete="off" class="box"/>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="password" value="<?php if($data != null) echo $data['password'] ?>" class="box" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Retry Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" name="passwordVarify" value="<?php if($data != null) echo $data['password'] ?>" class="box" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">Role</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="role" class="form-control">
                        <option <?php if($data != null && $data['role']=='administrator') echo 'selected="selected"'?> value="administrator">Administrator</option>
                        <option <?php if($data != null && $data['role']=='lurah') echo 'selected="selected"'?> value="lurah">Lurah</option>
                        <option <?php if($data != null && $data['role']=='kabidata') echo 'selected="selected"'?> value="kabidata">Kabid Data</option>
                        <option <?php if($data != null && $data['role']=='member') echo 'selected="selected"'?> value="member">Member</option>
                        <option <?php if($data != null && $data['role']=='sosialita') echo 'selected="selected"'?> value="sosialita">Ibu Sosialita</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 col-sm-2 control-label">ID Kelompok Warga</label>
                    <div class="col-sm-10">
                      <select class="form-control" name="id_kelompok" class="form-control">
                        <option <?php if($data != null && $data['id_kelompok']=='1') echo 'selected="selected"'?> value="1">1</option>
                        <option <?php if($data != null && $data['id_kelompok']=='2') echo 'selected="selected"'?> value="2">2</option>
                        <option <?php if($data != null && $data['id_kelompok']=='3') echo 'selected="selected"'?> value="3">3</option>
                        <option <?php if($data != null && $data['id_kelompok']=='4') echo 'selected="selected"'?> value="4">4</option>
                        <option <?php if($data != null && $data['id_kelompok']=='5') echo 'selected="selected"'?> value="5">5</option>
                      </select>
                    </div>
                  </div>
                  <a href="user_management.php" class='submit btn btn-danger'>Cancel</a>
                  <input type="submit" name='submit' value="Update" class='submit btn btn-primary'/><br />
                  <br />
                  <?php
                  if(isset($errMsg)){					
                    echo '<div class="alert alert-danger">'.$errMsg.'</div>';
                  }
                  ?>
                </form>
              </div>
            </section>
          </div>
        </div>
      </div>
      <!-- /content-panel -->
    </section>
  </section>

  <!--main content end-->
  <?php include 'footer.php'; ?>
</section>
</body>
</html>
