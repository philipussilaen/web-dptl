 <!DOCTYPE html>
 <html lang="en">
</head>
<body>
  <section id="container" >
    <?php include 'header.php'; ?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      ****************************s******************************************************************************************************************************* -->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
          <h3><i class="fa fa-angle-right"></i> Pusat Bantuan</h3>
          <div class="row mt">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <! -- BASIC BUTTONS -->
              <div class="showback">
                <h4 class="mb"><i class="fa fa-angle-right"></i> Form Bantuan</h4>
                <form class="form-horizontal tasi-form" method="get">
                  <div class="form-group has-success">
                    <label class="col-sm-2 control-label col-lg-2" for="inputSuccess">Nama</label>
                    <div class="col-lg-10">
                      <input type="text" class="form-control" id="inputSuccess">
                    </div>
                  </div>
                  <div class="form-group has-warning">
                    <label class="col-sm-2 control-label col-lg-2" for="inputWarning">Kontak</label>
                    <div class="col-lg-10">
                      <input type="text" class="form-control" id="inputWarning">
                    </div>
                  </div>
                  <div class="form-group has-error">
                    <label class="col-sm-2 control-label col-lg-2" for="inputError">Keterangan</label>
                    <div class="col-lg-10">
                      <input type="text" class="form-control" id="inputError">
                    </div>
                  </div>
                </form>
                <button type="button" class="btn btn-primary btn-lg btn-block" onclick="javascript:alert('Permintaan Akan Diprosess ...')">Submit</button>
              </div><!-- /showback -->
            </div><! --/col-lg-6 -->
            <div class="col-lg-6 col-md-6 col-sm-12">
              <! -- BUTTONS SIZES -->
              <div class="showback">
               <h4 class="mb"><i class="fa fa-angle-right"></i> Quick Request</h4>
               <button type="button" class="btn btn-round btn-lg btn-danger"  onclick="javascript:alert('Permintaan Akan Diprosess ...')">Panic Button !!!</button>                  
             </div><!-- /showback -->
             <div class="showback">
              <h4 class="mb"><i class="fa fa-angle-right"></i> Kontak Hansip</h4>
              <table class="table table-bordered table-striped table-condensed">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Pak Rudi</th>
                  </tr>
                  <tr>
                    <th>Kontak</th>
                    <th>08561234567890</th>
                  </tr>
                  <tr>
                    <th>Jam Jaga</th>
                    <th>18.00 - 23.00</th>
                  </tr>
                </thead>
              </table>
            </div><!--/showback -->
          </div><!-- /col-lg-6 -->
        </div><!--/ row -->
      </section><! --/wrapper -->
    </section><!-- /MAIN CONTENT -->
    <!--main content end-->
    <?php include 'footer.php'; ?>
  </section>


  <!--script for this page-->
  <script src="assets/js/sparkline-chart.js"></script>    
  <script src="assets/js/zabuto_calendar.js"></script>  

      <!-- <script type="text/javascript">
        $(document).ready(function () {
          var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Welcome to Dashgum!',
            // (string | mandatory) the text inside the notification
            text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
            // (string | optional) the image to display on the left
            image: 'assets/img/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
          });

          return false;
        });
      </script> -->

      <script type="application/javascript">
        $(document).ready(function () {
          $("#date-popover").popover({html: true, trigger: "manual"});
          $("#date-popover").hide();
          $("#date-popover").click(function (e) {
            $(this).hide();
          });

          $("#my-calendar").zabuto_calendar({
            action: function () {
              return myDateFunction(this.id, false);
            },
            action_nav: function () {
              return myNavFunction(this.id);
            },
            ajax: {
              url: "show_data.php?action=1",
              modal: true
            },
            legend: [
            {type: "text", label: "Special event", badge: "00"},
            {type: "block", label: "Regular event", }
            ]
          });
        });


        function myNavFunction(id) {
          $("#date-popover").hide();
          var nav = $("#" + id).data("navigation");
          var to = $("#" + id).data("to");
          console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
      </script>


    </body>
    </html>
