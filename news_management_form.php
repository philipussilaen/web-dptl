<!DOCTYPE html>
 <html lang="en">
 <body>
    <section id="container" >
<?php 
    include 'header.php';
    
    $isNew = true;
    
    $data = null;
    
    if(isset($_GET['id']) != ''){
        $isNew = false;

        $stmt = $connect->prepare('SELECT no, title, content, image_path FROM news WHERE no = :id');
        $stmt->execute(array(
            ':id' => $_GET['id']
        ));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
    }
        
	if(isset($_POST['submit'])) {
		$errMsg = '';

		// Getting data from FROM
		$no = $_POST['no'];
		$title = $_POST['title'];
		$content = $_POST['content'];
		
		
		$file_name = $_FILES['image']['name'];
		$file_size = $_FILES['image']['size'];
		$file_tmp = $_FILES['image']['tmp_name'];
		$file_type = $_FILES['image']['type'];
		$file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
		
		$expensions= array("jpeg","jpg","png");
		
		if(in_array($file_ext,$expensions)=== false){
		    $errMsg="extension not allowed, please choose a JPEG or PNG file.";
		}
		
		if($file_size > 2097152) {
		    $errMsg='File size must be excately 2 MB';
		}
		
		if(empty($errors)==true) {
		    move_uploaded_file($file_tmp,"./images/".$file_name);
		    echo "Success";
		}else{
		    print_r($errors);
		}
		

		$imagePath = "./images/".$file_name;
		
		if($errMsg == '') {
			try {
			    if ($isNew){
			        $stmt = $connect->prepare('INSERT INTO news (title, content, image_path) VALUES (:title, :content, :image_path)');
			        $stmt->execute(array(
			            ':title' => $title,
			            ':content' => $content,
			            ':image_path' => $imagePath
			        ));
			        echo "<script>location.href='news_management.php?action=News successfully created';</script>";
			    }else{
			        $sql = "UPDATE news SET title = :title, content = :content, image_path = :image_path WHERE no = :no";
			        $stmt = $connect->prepare($sql);
			        $stmt->execute(array(
			            ':title' => $title,
			            ':content' => $content,
			            ':image_path' => $imagePath,
			            ':no' => $no
			        ));
			        echo "<script>location.href='news_management.php?action=News successfully updated';</script>";
			    }
			}
			catch(PDOException $e) {
				$errMsg = $e->getMessage();
			}
		}
	}
?>

      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> Form News</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <div class="col-lg-12">
              	<h4><i class="fa fa-angle-right"></i> News</h4>
              </div>
              <section id="unseen">
              	<div class="form-panel">
					<form class="form-horizontal style-form" method="post" enctype = "multipart/form-data" action="">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Title</label>
                              <div class="col-sm-10">
                                  <input type="hidden" name="no" value="<?php if ($data != null) echo $data['no']; ?>" />
                                  <input type="text" class="form-control" name="title" value="<?php if($data != null) echo $data['title']; ?>" autocomplete="off" class="box"/>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Content</label>
                              <div class="col-sm-10">
                              	  <textarea class="form-control" name="content" rows="4" cols="2"><?php if($data != null) echo $data['content']; ?></textarea>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Image</label>
                              <div class="col-sm-10">
                                  <input type="file" name="image" />
                              </div>
                          </div>
                          
                          <a href="news_management.php" class='submit btn btn-danger'>Cancel</a>
                    	  <input type="submit" name='submit' value="Update" class='submit btn btn-primary'/><br />
                    	<br />
                       	<?php
        				    if(isset($errMsg)){					
        					   echo '<div class="alert alert-danger">'.$errMsg.'</div>';
        				    }
			             ?>
                     </form>
                   </div>
              </section>
            </div>
          </div>
         </div>
            <!-- /content-panel -->
          </section>
        </section>

        <!--main content end-->
        <?php include 'footer.php'; ?>
      </section>
    </body>
</html>
