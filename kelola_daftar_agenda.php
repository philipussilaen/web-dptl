<!DOCTYPE html>
 <html lang="en">
 <body>
    <section id="container" >
      <?php include 'header.php'; ?>
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> Kelola Agenda</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
            <?php 
                if(isset($_GET['action']) != ''){
                    echo '<div class="col-lg-12"> <div class="alert alert-info"><b>Info ! </b>'.$_GET['action'].'.</div></div>';
                }
            
            ?>
              <div class="col-lg-6">
                <h4><i class="fa fa-angle-right"></i> Agenda</h4>
              </div>
              <div class="col-lg-6">
                <a type="button" href="daftar_agenda_form.php" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add Agenda</a>
              </div>
              <section id="unseen">
                <table class="table table-bordered table-striped table-condensed">
                  <thead>
                    <tr>
                      <th class="numeric">No.</th>
                      <th>Nama agenda</th>
                      <th>Deskripsi</th>
                      <th>Tanggal mulai</th>
                      <th>Tanggal berakhir</th>
                      <th>lokasi</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php 
                      $i = 0; 
                      foreach ($connect->query("SELECT * FROM agenda") as $rows) {
                        $i++;
                        echo '<tr>';
                        echo '<td>' . $i . '</td>';
                        echo '<td>' . $rows['nama_agenda'] . '</td>';
                        echo '<td>' . $rows['deskripsi'] . '</td>';
                        echo '<td>' . $rows['tanggal_mulai'] . '</td>';
                        echo '<td>' . $rows['tanggal_berakhir'] . '</td>';
                        echo '<td>' . $rows['lokasi'] . '</td>';
                        echo '<td><a  class="btn btn-theme02" href="daftar_agenda_form.php?id='.$rows['id_agenda'].'"><i class="glyphicon glyphicon-edit"></i> Update</a> <a  class="btn btn-theme04" href="daftar_agenda_update.php?id='.$rows['id_agenda'].'"><i class="glyphicon glyphicon-trash"></i> Delete</a></td>';
                        echo '</tr>';
                      }  
                      ?>              
                  </tbody>
                </table>
              </section>
            </div>
          </div>
         </div>
            <!-- /content-panel -->
          </section>
        </section>

        <!--main content end-->
        <?php include 'footer.php'; ?>
      </section>
   </body>
</html>
