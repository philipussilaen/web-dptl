<!DOCTYPE html>
<html lang="en">
<body>

 <!--external css-->
 <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
 <link href="assets/js/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
 <section id="container" >
  <?php include 'header.php'; ?>

  <?php 

  $news = $connect->query('SELECT no, title, content, image_path FROM news');

  ?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper site-min-height">
            <h3><i class="fa fa-angle-right"></i> Halaman Administrator</h3>
            <div class="row mt">
              <div class="col-lg-12">
          <! -- 5TH ROW OF PANELS -->
          <div class="row">
            <div class="col-md-4 col-sm-4 mb">
                <div class="green-panel pn">
                  <div class="green-header">
                    <h5>User Management</h5>
                            </div>
                            <h1 class="mt"><a href="http://env-dev.tk/dtpl/user_management.php"><i class="fa fa-user fa-3x"></i></a></h1>
                <p>Mengelola pengguna aplikasi</p> 
                <footer>
                  <!-- <div class="centered">
                    <h5><i class="fa fa-trophy"></i> 17,988</h5> 
                  </div> -->
                </footer>
                </div><! -- /darkblue panel -->
              </div><!-- /col-md-4 -->
          
              <div class="col-md-4 col-sm-4 mb">
                <div class="green-panel pn">
                  <div class="green-header">
                    <h5>News Management</h5>
                            </div>
                            <h1 class="mt"><a href="http://env-dev.tk/dtpl/news_management.php"><i class="fa fa-list-alt fa-3x"></i></a></h1> 
                <p>Mengelola berita dan informasi</p> 
                <footer>
                  <!-- <div class="centered">
                    <h5><i class="fa fa-trophy"></i> 17,988</h5> 
                  </div> -->
                </footer>
                </div><! -- /darkblue panel -->
              </div><!-- /col-md-4 -->
              <div class="col-md-4 col-sm-4 mb">
                <div class="green-panel pn">
                  <div class="green-header">
                    <h5>Kelompok Warga</h5>
                            </div>
                            <h1 class="mt"><a href="http://env-dev.tk/dtpl/daftar_kelompok_warga.php"><i class="fa fa-users fa-3x"></i></a></h1>
                <p>Mengelola kelompok warga</p> 
                <footer>
                 <!--  <div class="centered">
                    <h5><i class="fa fa-trophy"></i> 17,988</h5> 
                  </div> -->
                </footer>
                </div><! -- /darkblue panel -->
              </div><!-- /col-md-4 -->
          </div><!-- /END 5TH ROW OF PANELS -->

          <! -- 5TH ROW OF PANELS -->
          <div class="row">
            <div class="col-md-4 col-sm-4 mb">
                <div class="green-panel pn">
                  <div class="green-header">
                    <h5>Kelola Agenda</h5>
                            </div>
                            <h1 class="mt"><a href="http://env-dev.tk/dtpl/kelola_daftar_agenda.php"><i class="fa fa-calendar fa-3x"></i></a></h1>
                <p>Mengelola agenda RT</p> 
                <footer>
                  <!-- <div class="centered">
                    <h5><i class="fa fa-trophy"></i> 17,988</h5> 
                  </div> -->
                </footer>
                </div><! -- /darkblue panel -->
              </div><!-- /col-md-4 -->
          
              <div class="col-md-4 col-sm-4 mb">
                <div class="green-panel pn">
                  <div class="green-header">
                    <h5>Permintaan Bantuan</h5>
                            </div>
                            <h1 class="mt"><a href="http://env-dev.tk/dtpl/daftar_permintaan_bantuan.php"><i class="fa fa-phone-square fa-3x"></i></a></h1>
                <p>Mengelola permintaan bantuan</p> 
                <footer>
                  <!-- <div class="centered">
                    <h5><i class="fa fa-trophy"></i> 17,988</h5> 
                  </div> -->
                </footer>
                </div><! -- /darkblue panel -->
              </div><!-- /col-md-4 -->

              <div class="col-md-4 col-sm-4 mb">
                <div class="green-panel pn">
                  <div class="green-header">
                    <h5>Pasar Online </h5>
                            </div>
                            <h1 class="mt"><a href="http://env-dev.tk/dtpl/pasar_online_management.php"><i class="fa fa-shopping-cart fa-3x"></i></a></h1>
                <p>Mengelola daftar pasar online</p> 
                <footer>
                  <!-- <div class="centered">
                    <h5><i class="fa fa-trophy"></i> 17,988</h5> 
                  </div> -->
                </footer>
                </div><! -- /darkblue panel -->
              </div><!-- /col-md-4 -->
              
          </div><!-- /END 5TH ROW OF PANELS -->
              </div>
            </div>
      
    </section><! --/wrapper -->
      </section><!-- /MAIN CONTENT -->

    <!--main content end-->
    <?php include 'footer.php'; ?>
  </section>


  <!--script for this page-->
  <script src="assets/js/sparkline-chart.js"></script>    
  <script src="assets/js/zabuto_calendar.js"></script>  

  <script type="text/javascript">
    $(document).ready(function () {
      var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Welcome to eRTe Online!',
            // (string | mandatory) the text inside the notification
            text: 'Website ini berisi informasi dan kegiatan penting di RT maupun Kelurahan. Anda juga dapat menggunakan fitur menarik seperti belanja dan arisan Online serta memberikan peringatan dalam keadaan darurat.',
            // (string | optional) the image to display on the left
            image: 'assets/img/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
          });

      return false;
    });
  </script>

  <script type="application/javascript">
    $(document).ready(function () {
      $("#date-popover").popover({html: true, trigger: "manual"});
      $("#date-popover").hide();
      $("#date-popover").click(function (e) {
        $(this).hide();
      });

      $("#my-calendar").zabuto_calendar({
        action: function () {
          return myDateFunction(this.id, false);
        },
        action_nav: function () {
          return myNavFunction(this.id);
        },
        ajax: {
          url: "show_data.php?action=1",
          modal: true
        },
        legend: [
        {type: "text", label: "Special event", badge: "00"},
        {type: "block", label: "Regular event", }
        ]
      });
    });


    function myNavFunction(id) {
      $("#date-popover").hide();
      var nav = $("#" + id).data("navigation");
      var to = $("#" + id).data("to");
      console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
    }
  </script>
  <!-- js placed at the end of the document so the pages load faster -->
  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="assets/js/fullcalendar/fullcalendar.min.js"></script>    
  <script src="assets/js/bootstrap.min.js"></script>
  <script class="include" type="text/javascript" src="assets/js/jquery.dcjqaccordion.2.7.js"></script>
  <script src="assets/js/jquery.scrollTo.min.js"></script>
  <script src="assets/js/jquery.nicescroll.js" type="text/javascript"></script>


  <!--common script for all pages-->
  <script src="assets/js/common-scripts.js"></script>

  <!--script for this page-->
  <script src="assets/js/calendar-conf-events.js"></script>    

  <script>
      //custom select box

      $(function(){
        $("select.styled").customSelect();
      });

    </script>

  </body>
  </html>
