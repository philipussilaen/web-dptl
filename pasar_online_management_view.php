<!DOCTYPE html>
 <html lang="en">
 <body>
    <section id="container" >
<?php 
    include 'header.php';
    
    $isNew = true;
    
    $data = null;
    
    if(isset($_GET['id']) != ''){
        $isNew = false;

        $stmt = $connect->prepare('SELECT no, title, content, contact, price, image_path, kategori FROM pasar_online WHERE no = :id');
        $stmt->execute(array(
            ':id' => $_GET['id']
        ));
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $stmt = $connect->prepare('SELECT * FROM kategori WHERE code = :code');
        $stmt->execute(array(
            ':code' => $data['kategori']
        ));
        $data_kategori = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $komentar = $_POST['komentar'];
        $_POST['komentar'] = '';
        
        if ($komentar != ""){
            $stmt = $connect->prepare('INSERT INTO komentar_pasar_online (username, komentar, pasar_online_no, timestamp) VALUES (:username, :komentar, :pasar_online_no, :timestamp)');
            $stmt->execute(array(
                ':username' => $_SESSION['fullname'],
                ':komentar' => $komentar,
                ':pasar_online_no' => $_GET['id'],
                ':timestamp' => date("d/m/Y h:i:sa")
            ));
            echo "<script>document.getElementById('komentar').value=''</script>";
        }
    }
        
?>

      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> View Pasar Online</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <div class="col-lg-12">
              	<h4><i class="fa fa-angle-right"></i> Pasar Online</h4>
              </div>
              <section id="unseen">
              	<div class="form-panel">
					<div class="form-horizontal style-form">
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Image</label>
                              <div class="col-sm-10">
                                  <img class="img-responsive" height="100" width="100" src="<?php echo $data['image_path']?>" alt="">
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Title</label>
                              <div class="col-sm-10">
                                  <input type="hidden" name="no" value="<?php if ($data != null) echo $data['no']; ?>" />
                                  <?php if($data != null) echo $data['title']; ?>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Content</label>
                              <div class="col-sm-10">
                              	  <?php if($data != null) echo $data['content']; ?>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Price</label>
                              <div class="col-sm-10">
                              	  <?php if($data != null) echo $data['price']; ?>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Contact</label>
                              <div class="col-sm-10">
                              	  <?php if($data != null) echo $data['contact']; ?>
                              </div>
                          </div>
                          <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Category</label>
                              <div class="col-sm-10">
                              <?php 
                                    echo $data_kategori['label']
                              ?>
                              </div>
                          </div>
                     </div>
                   </div>
                   
                 <div class="form-panel">
                     <div class="form-horizontal style-form">
                     <?php 
                     $i = 0;
                     $class = '';
                     foreach ($connect->query("SELECT * FROM komentar_pasar_online WHERE pasar_online_no = ".$_GET['id']." order by id desc") as $rows) {
                         if ($i%2 == 1){
                             $class = 'alert-info';
                         } else {
                             $class = 'alert-warning';
                         }
                         echo '<div class="alert '.$class.'"> <b>'.$rows['username'].'</b> '.$rows['komentar'].' <br/> <i class="pull-left">'.$rows['timestamp'].'</i> <br/> </div>';
                         $i++;
                     }
                     ?>
                     </div>
                 </div>
                     
                 <div class="form-panel">
                     <form class="form-horizontal style-form" method="post" action="">
                        <div class="form-group">
                              <label class="col-sm-2 col-sm-2 control-label">Komentar</label>
                              <div class="col-sm-10">
                              	  <textarea class="form-control" required name="komentar" rows="4" cols="2"></textarea>
                              </div>
                          </div>
                          
                         <a href="index.php" class='submit btn btn-danger'>Cancel</a>
                         <input type="submit" name='submit' value="Update" class='submit btn btn-primary'/><br />
                     </form>
                 </div>
                 
              </section>
            </div>
          </div>
         </div>
          </section>
        </section>

        <!--main content end-->
        <?php include 'footer.php'; ?>
      </section>
    </body>
</html>
