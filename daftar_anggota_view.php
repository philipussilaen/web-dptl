<!DOCTYPE html>
 <html lang="en">
 <body>
    <section id="container" >
<?php 
    include 'header.php';
    
    $data = null;
    
?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> Daftar Anggota Kelompok Warga</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
              <h4><i class="fa fa-angle-right"></i> Anggota Kelompom Warga</h4>
              <section id="unseen">
                <table class="table table-bordered table-striped table-condensed">
                  <thead>
                    <tr>
                      <th class="numeric">ID</th>
                      <th>Nama Warga</th>
                      <th>Email</th>
                      <th>Role</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                      foreach ($connect->query("SELECT * FROM user WHERE id_kelompok = " . $_GET['id_kelompok']) as $rows) {
                        echo '<tr>';
                        echo '<td>' . $rows['id'] . '</td>';
                        echo '<td>' . $rows['fullname'] . '</td>';
                        echo '<td>' . $rows['email'] . '</td>';
                        echo '<td>' . $rows['role'] . '</td>';
                        echo '</tr>';
                      }
                      ?>              
                  </tbody>
                </table>
              </section>
            </div>
          </div>
         </div>
            <!-- /content-panel -->
          </section>
        </section>

        <!--main content end-->
        <?php include 'footer.php'; ?>
      </section>


      <!--script for this page-->
      <script src="assets/js/sparkline-chart.js"></script>    
      <script src="assets/js/zabuto_calendar.js"></script>  

      <!-- <script type="text/javascript">
        $(document).ready(function () {
          var unique_id = $.gritter.add({
            // (string | mandatory) the heading of the notification
            title: 'Welcome to Dashgum!',
            // (string | mandatory) the text inside the notification
            text: 'Hover me to enable the Close Button. You can hide the left sidebar clicking on the button next to the logo. Free version for <a href="http://blacktie.co" target="_blank" style="color:#ffd777">BlackTie.co</a>.',
            // (string | optional) the image to display on the left
            image: 'assets/img/ui-sam.jpg',
            // (bool | optional) if you want it to fade out on its own or just sit there
            sticky: true,
            // (int | optional) the time you want it to be alive for before fading out
            time: '',
            // (string | optional) the class name you want to apply to that specific message
            class_name: 'my-sticky-class'
          });

          return false;
        });
      </script> -->

      <script type="application/javascript">
        $(document).ready(function () {
          $("#date-popover").popover({html: true, trigger: "manual"});
          $("#date-popover").hide();
          $("#date-popover").click(function (e) {
            $(this).hide();
          });

          $("#my-calendar").zabuto_calendar({
            action: function () {
              return myDateFunction(this.id, false);
            },
            action_nav: function () {
              return myNavFunction(this.id);
            },
            ajax: {
              url: "show_data.php?action=1",
              modal: true
            },
            legend: [
            {type: "text", label: "Special event", badge: "00"},
            {type: "block", label: "Regular event", }
            ]
          });
        });


        function myNavFunction(id) {
          $("#date-popover").hide();
          var nav = $("#" + id).data("navigation");
          var to = $("#" + id).data("to");
          console.log('nav ' + nav + ' to: ' + to.month + '/' + to.year);
        }
      </script>
    </body>
    </html>