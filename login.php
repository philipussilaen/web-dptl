<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/style-responsive.css" rel="stylesheet">
	<link href="assets/css/custom.css" rel="stylesheet">
	
    <title>eRTe Application</title>
  </head>

  <body>

<?php 

    require(dirname(__FILE__)."/db/db.php");
    
    if(isset($_POST['login'])) {
        $errMsg = '';
        
        // Get data from FORM
        $username = $_POST['username'];
        $password = $_POST['password'];
        
        if($username == '')
            $errMsg = 'Enter username';
            if($password == '')
                $errMsg = 'Enter password';
                
                if($errMsg == '') {
                    try {
                        $stmt = $connect->prepare('SELECT username, password, fullname, role FROM user WHERE username = :username');
                        $stmt->execute(array(
                            ':username' => $username
                        ));
                        $data = $stmt->fetch(PDO::FETCH_ASSOC);
                        
                        if($data == false){
                            $errMsg = "User $username not found.";
                        }
                        else {
                            if($password == $data['password']) {
                                $_SESSION['username'] = $data['username'];
                                $_SESSION['password'] = $data['password'];
                                $_SESSION['role'] = $data['role'];
                                $_SESSION['fullname'] = $data['fullname'];
                                
                                header('Location: index.php');
                                exit;
                            }
                            else
                                $errMsg = 'Password not match.';
                        }
                    }
                    catch(PDOException $e) {
                        $errMsg = $e->getMessage();
                    }
                }
       }
?>

      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->

	  <div id="login-page">
	  	<div class="container">
		      <form class="form-login" action="" method="post">
		        <h2 class="form-login-heading">eRTe - Login Form</h2>
		        <div class="login-wrap">
		            <input type="text" name="username" class="form-control" placeholder="User ID" 
		            	value="<?php if(isset($_POST['username'])) echo $_POST['username'] ?>"
		                  autofocus>
		            <br>
		            <input type="password" name="password" class="form-control"
		            	<?php if(isset($_POST['password'])) echo $_POST['password'] ?>
		            	placeholder="Password">
					<br />
		            <input type="submit" name='login' value="SIGN IN" class="btn btn-theme btn-block"/><br />
		            <hr />
		            
		           	<?php
        				if(isset($errMsg)){					
        					echo '<div class="alert alert-danger">'.$errMsg.'</div>';
        				}
			         ?>
		        </div>
		      </form>	  	
	  	
	  	</div>
	  </div>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="assets/js/jquery.backstretch.min.js"></script>
    <script>
        $.backstretch("assets/img/login-bg.jpg", {speed: 500});
    </script>


  </body>
</html>
