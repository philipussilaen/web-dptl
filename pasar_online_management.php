<!DOCTYPE html>
 <html lang="en">
 <body>
    <section id="container" >
      <?php include 'header.php'; ?>
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> Daftar Pasar Online</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
            <?php 
                if(isset($_GET['action']) != ''){
                    echo '<div class="col-lg-12"> <div class="alert alert-info"><b>Info ! </b>'.$_GET['action'].'.</div></div>';
                }
            
            ?>
              <div class="col-lg-6">
              	<h4><i class="fa fa-angle-right"></i> Pasar Online</h4>
              </div>
              <div class="col-lg-6">
              	<a type="button" href="pasar_online_management_form.php" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add Pasar Online</a>
              </div>
              <section id="unseen">
                <table class="table table-bordered table-striped table-condensed">
                  <thead>
                    <tr>
                      <th class="numeric">No.</th>
                      <th>Title</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                      $i = 0;
                      foreach ($connect->query("SELECT * FROM pasar_online") as $rows) {
                        $i++;
                        echo '<tr>';
                        echo '<td>' . $i . '</td>';
                        echo '<td>' . $rows['title'] . '</td>';
                        echo '<td><a class="btn btn-theme03" href="pasar_online_management_view.php?id='.$rows['no'].'"><i class="glyphicon glyphicon-eye-open"></i> View</a> <a  class="btn btn-theme02" href="pasar_online_management_form.php?id='.$rows['no'].'"><i class="glyphicon glyphicon-edit"></i> Update</a> <a  class="btn btn-theme04" href="pasar_online_management_delete.php?id='.$rows['no'].'"><i class="glyphicon glyphicon-trash"></i> Delete</a></td>';
                        echo '</tr>';
                      }
                      ?>              
                  </tbody>
                </table>
              </section>
            </div>
          </div>
         </div>
            <!-- /content-panel -->
          </section>
        </section>

        <!--main content end-->
        <?php include 'footer.php'; ?>
      </section>
   </body>
</html>
