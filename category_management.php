<!DOCTYPE html>
 <html lang="en">
 <body>
    <section id="container" >
      <?php include 'header.php'; ?>
      <section id="main-content">
        <section class="wrapper">
         <h3><i class="fa fa-angle-right"></i> Daftar Category</h3>
         <div class="row mt">
          <div class="col-lg-12">
            <div class="content-panel">
            <?php 
                if(isset($_GET['action']) != ''){
                    echo '<div class="col-lg-12"> <div class="alert alert-info"><b>Info ! </b>'.$_GET['action'].'.</div></div>';
                }
            
            ?>
              <div class="col-lg-6">
              	<h4><i class="fa fa-angle-right"></i> Category</h4>
              </div>
              <div class="col-lg-6">
              	<a type="button" href="category_management_form.php" class="btn btn-primary pull-right"><i class="glyphicon glyphicon-plus"></i> Add Category</a>
              </div>
              <section id="unseen">
                <table class="table table-bordered table-striped table-condensed">
                  <thead>
                    <tr>
                      <th class="numeric">No.</th>
                      <th>Title</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php
                      $i = 0;
                      foreach ($connect->query("SELECT * FROM kategori") as $rows) {
                        $i++;
                        echo '<tr>';
                        echo '<td>' . $i . '</td>';
                        echo '<td>' . $rows['label'] . '</td>';
                        echo '<td><a  class="btn btn-theme02" href="category_management_form.php?id='.$rows['code'].'"><i class="glyphicon glyphicon-edit"></i> Update</a></td>';
                        echo '</tr>';
                      }
                      ?>              
                  </tbody>
                </table>
              </section>
            </div>
          </div>
         </div>
            <!-- /content-panel -->
          </section>
        </section>

        <!--main content end-->
        <?php include 'footer.php'; ?>
      </section>
   </body>
</html>
